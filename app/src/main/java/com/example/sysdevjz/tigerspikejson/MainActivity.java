package com.example.sysdevjz.tigerspikejson;

/**
 * MainActivity, which is the first screen the user sees
 * This contains the gridview from which they can scroll through the images
 *
 * Created by sysdevjz on 10/10/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private GridView mGridView;
    private ProgressBar mProgressBar;

    private AdapterGridView mGridAdapter;
    private ArrayList<Grid> mGridData;

    // Using the old API JSON which does not require an API Key
    private String URL_BEGIN = "https://api.flickr.com/services/feeds/photos_public.gne?nojsoncallback=1&format=json&tags=";

    // Since the string Tags has been separated from the main URL this could be used for searching specific tags
    private String TAGS = "cars";

    private String FEED_URL = URL_BEGIN + TAGS ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new AdapterGridView(this, R.layout.grid_item_layout, mGridData);
        mGridView.setAdapter(mGridAdapter);

        //Grid view click event
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Get item at position
                Grid item = (Grid) parent.getItemAtPosition(position);

                Intent mainActivity = new Intent(MainActivity.this, InfoActivity.class);
                ImageView imageView = (ImageView) v.findViewById(R.id.grid_item_image);


                int[] screenLocation = new int[2];
                imageView.getLocationOnScreen(screenLocation);

                //Pass the image title and url to InfoActivity
                mainActivity.putExtra("left", screenLocation[0]).
                        putExtra("top", screenLocation[1]).
                        putExtra("width", imageView.getWidth()).
                        putExtra("height", imageView.getHeight()).
                        putExtra("title", item.getTitle()).
                        putExtra("tags", item.getTags()).
                        putExtra("image", item.getImage());

                //Start details activity
                startActivity(mainActivity);
            }
        });

        //Start download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    class ProxyAuthenticator extends Authenticator {

        // Used for proxy authentication for the Morrisons network, it takes parameters
        // which are defined in the AsyncHttpTask

        private String user, password;

        public ProxyAuthenticator(String user, String password) {
            this.user = user;
            this.password = password;
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(user, password.toCharArray());
        }
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
//                Authenticator.setDefault(new ProxyAuthenticator("SVC_XXCIUSER", "Holly7time2soon"));
//                System.setProperty("http.proxyHost", "10.7.128.15");
//                System.setProperty("http.proxyPort", "8080");

                // gets the feed url which is the flickr api json
                URL url = new URL(FEED_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                int statusCode = conn.getResponseCode();
                Log.d("statuscode", "status" + statusCode);

                /* Checks to see if it returns a 200 or 201 response, if it does
                then it returns a successful status
                 */
                if (conn.getResponseCode() == 201 || conn.getResponseCode() == 200) {
                    String response = streamToString(conn.getInputStream());
                    parseResult(response);

                    // Response Successful
                    result = 1;
                } else {
                    // Response Failure
                    result = 0;
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {

            //Update the user interface after the download has compeleted
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
            } else {
                Toast.makeText(MainActivity.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }

            mProgressBar.setVisibility(View.GONE);
        }
    }


    String streamToString(InputStream stream) throws IOException {
        //Gets the data from the input stream into a buffered reaer
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";

        // Creates a string from the results
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }


    /**
     * Parses through the results of the JSON
     *
     * @param result
     */
    private void parseResult(String result) {
        try {

            // Gets JSON Object from the results
            JSONObject response = new JSONObject(result);

            // Gets the JSON Array called items as it is in the flickr JSON api
            JSONArray posts = response.optJSONArray("items");
            Grid item;

            // Loops through the results from the JSON Array
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);

                // Gets the description and tags string
                String title = post.optString("title");
                String tags = post.optString("tags");
                item = new Grid();
                item.setTitle(title);
                item.setTags("Tags: " + tags);

                /* Gets the media json object called media, and the string called m
                   which contains the image URL */

                item.setImage(post.getJSONObject("media").getString("m"));

                // adds the items to the grid
                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}