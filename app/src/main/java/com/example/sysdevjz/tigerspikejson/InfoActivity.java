package com.example.sysdevjz.tigerspikejson;

/**
 * This is the view the user sees when they click on an image from the imageview
 * This controls what is displayed on the view as well as the animations
 * when clicking an image from the grid and going back also.
 *
 * Parts of this have been taken from the internet and modified for this purpose
 *
 * Created by sysdevjz on 10/10/2016.
 */


import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class InfoActivity extends AppCompatActivity {
    private static final int ANIM_DURATION = 600;
    private TextView titleTV;
    private TextView tagsTV;
    private ImageView imageIV;

    private int xTranslation;
    private int yTranslation;
    private float xScale;
    private float yScale;

    private FrameLayout mainLayout;
    private ColorDrawable colorDrawable;

    private int thumbnailTop;
    private int thumbnailLeft;
    private int thumbnailWidth;
    private int thumbnailHeight;

    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting details screen layout
        setContentView(R.layout.activity_details_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        //retrieves the thumbnail data
        Bundle bundle = getIntent().getExtras();
        thumbnailTop = bundle.getInt("top");
        thumbnailLeft = bundle.getInt("left");
        thumbnailWidth = bundle.getInt("width");
        thumbnailHeight = bundle.getInt("height");

        String title = bundle.getString("title");
        String tags = bundle.getString("tags");
        String image = bundle.getString("image");

        //initialize and set the image description
        titleTV = (TextView) findViewById(R.id.title);
        titleTV.setText(Html.fromHtml(title));

        //tags
        tagsTV = (TextView) findViewById(R.id.tags);
        tagsTV.setText(Html.fromHtml(tags));

        //Set image url
        imageIV = (ImageView) findViewById(R.id.grid_item_image);
        Picasso.with(this).load(image).into(imageIV);

        //Set the background color to black
        mainLayout = (FrameLayout) findViewById(R.id.main_background);
        colorDrawable = new ColorDrawable(Color.BLACK);
        mainLayout.setBackground(colorDrawable);

        if (savedInstanceState == null) {
            ViewTreeObserver observer = imageIV.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    imageIV.getViewTreeObserver().removeOnPreDrawListener(this);

                    int[] screenLocation = new int[2];
                    imageIV.getLocationOnScreen(screenLocation);
                    xTranslation = thumbnailLeft - screenLocation[0];
                    yTranslation = thumbnailTop - screenLocation[1];

                    xScale = (float) thumbnailWidth / imageIV.getWidth();
                    yScale = (float) thumbnailHeight / imageIV.getHeight();

                    enterAnimation();

                    return true;
                }
            });
        }
    }

    /**
     * Animation
     */
    public void enterAnimation() {
        imageIV.setPivotX(0);
        imageIV.setPivotY(0);
        imageIV.setScaleX(xScale);
        imageIV.setScaleY(yScale);
        imageIV.setTranslationX(xTranslation);
        imageIV.setTranslationY(yTranslation);

        TimeInterpolator sDecelerator = new DecelerateInterpolator();

        imageIV.animate().setDuration(ANIM_DURATION).scaleX(1).scaleY(1).
                translationX(0).translationY(0).setInterpolator(sDecelerator);

        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0, 255);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();

    }

    /**
     * @param endAction This occurs on switching the activity
     */
    public void exitAnimation(final Runnable endAction) {

        TimeInterpolator sInterpolator = new AccelerateInterpolator();
        imageIV.animate().setDuration(ANIM_DURATION).scaleX(xScale).scaleY(yScale).
                translationX(xTranslation).translationY(yTranslation)
                .setInterpolator(sInterpolator).withEndAction(endAction);

        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

    @Override
    public void onBackPressed() {
        exitAnimation(new Runnable() {
            public void run() {
                finish();
            }
        });
    }
}
