package com.example.sysdevjz.tigerspikejson;

/**
 * Created by sysdevjz on 10/10/2016.
 */

        import java.util.ArrayList;

        import android.app.Activity;
        import android.content.Context;
        import android.text.Html;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.squareup.picasso.Picasso;

public class AdapterGridView extends ArrayAdapter<Grid> {

    private Context context;

    private ArrayList<Grid> dataGrid = new ArrayList<Grid>();
    private int resLayoutID;

    public AdapterGridView(Context context, int resLayoutID, ArrayList<Grid> dataGrid) {
        super(context, resLayoutID, dataGrid);
        this.resLayoutID = resLayoutID;
        this.context = context;
        this.dataGrid = dataGrid;
    }


    /**
     * The grid data is reloaded and refreshed
     *
     * @param dataGrid
     */
    public void setGridData(ArrayList<Grid> dataGrid) {
        this.dataGrid = dataGrid;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        tiView holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resLayoutID, parent, false);
            holder = new tiView();
            holder.iView = (ImageView) row.findViewById(R.id.grid_item_image);
            row.setTag(holder);
        } else {
            holder = (tiView) row.getTag();
        }

        Grid item = dataGrid.get(position);

        Picasso.with(context).load(item.getImage()).into(holder.iView);
        return row;
    }

    static class tiView {
        TextView titleTV;
        ImageView iView;
    }
}