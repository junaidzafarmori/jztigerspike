package com.example.sysdevjz.tigerspikejson;

/**
 * Getters and setters for return and setting tags, titles and images
 *
 * Created by sysdevjz on 10/10/2016.
 */

public class Grid {
    private String image;
    private String title;
    private String tags;

    public Grid() {
        super();
    }

    // Sets the tag
    public void setTags(String tags) {
        this.tags = tags;
    }

    // Gets the tag from the JSON result
    public String getTags() {
        return tags;
    }

    // Gets the title from the JSON result
    public String getTitle() {
        return title;
    }

    // Sets the title
    public void setTitle(String title) {
        this.title = title;
    }

    // Gets the image URL from the JSON result
    public String getImage() {
        return image;
    }

    // Sets the image URL
    public void setImage(String image) {
        this.image = image;
    }
}
